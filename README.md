# `unreal-snippets.vim`

> Collection of vim commands from Andrew Forsyth's
> [Sublime Text 3 snippets](https://github.com/awforsythe/sublime-unreal-snippets)
> for writing Unreal Engine 4 game code.

Install using your preferred vim plugin installer.

Commands:

- `UEPROJ` -- boilerplate .uproject file (`Project.uproject`)
- `UETARGET` -- project-level target rules file (`Source/Project.Target.cs`)
- `UEBUILD` -- module-level build rules file (`Source/ProjectCore/ProjectCore.Build.cs`)
- `UEMODULEH` -- module public header file (`Source/ProjectCore/Public/ProjectCore.h`)
- `UEMODULECP` -- primary game module implementation (one per project)
- `UELOGH` -- module-level log category in module-private header
  (`Source/ProjectCore/Private/Log.h`)
- `UELOGC` -- module-level log category in a cpp file
  (`Source/ProjectCore/Private/Log.cpp`)
- `UELOGL` -- write a single `UE_LOG` line using the module-level log category
- ... TODO: more
