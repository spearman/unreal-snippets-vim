" ------------------------------------------------------------------------------
" exit when your app has already been loaded (or "compatible" mode set)
if exists("g:loaded_unreal_snippets_vim") || &cp
  finish
endif
let g:loaded_unreal_snippets_vim = 1  " version number
let s:keepcpo                    = &cpo
set cpo&vim

let ueproj = expand ("%:t:r")
command UEPROJ execute "normal! i
  \{\n
  \  \"FileVersion\": 3,\n
  \  \"EngineAssociation\": \"4.25\",\n
  \  \"Category\": \"\",\n
  \  \"Description\": \"\",\n
  \  \"Modules\": [\n
  \    {\n
  \      \"Name\": \"" . ueproj . "Core\",\n
  \      \"Type\": \"Runtime\",\n
  \      \"LoadingPhase\": \"Default\"\n
  \    }\n
  \  ]\n
  \}"

let uerules = expand ("%:t:r:r")
command UETARGET execute "setlocal indentexpr= | normal! i
  \using UnrealBuildTool;\n
  \public class " . uerules . "Target : TargetRules {\n
  \  public " . uerules . "Target (TargetInfo Target) : base (Target) {\n
  \    Type = TargetType.Game;\n
  \    DefaultBuildSettings = BuildSettingsVersion.V2;\n
  \    ExtraModuleNames.AddRange (new string[] { \"" . uerules . "Core\" });\n
  \  }\n
  \}"

command UEBUILD execute "setlocal indentexpr= | normal! i
  \using UnrealBuildTool;\n
  \public class " . uerules . " : ModuleRules {\n
  \  public " . uerules . " (ReadOnlyTargetRules Target) : base (Target) {\n
  \    PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;\n
  \    bEnforceIWYU = true;\n
  \    PublicDependencyModuleNames.AddRange (new string[] {\n
  \      \"Core\", \"CoreUObject\", \"Engine\"\n
  \    });\n
  \    PrivateDependencyModuleNames.AddRange (new string[] { });\n
  \  }\n
  \}"

let uesource = expand ("%:t:r")
command UEMODULEH execute "setlocal nocindent | normal! i
  \#pragma once\n
  \\n
  \#include \"CoreMinimal.h\"\n
  \#include \"Modules/ModuleInterface.h\"\n
  \\n
  \class F" . uesource . " : public IModuleInterface {\n
  \public:\n
  \  static inline F" . uesource . "& Get() {\n
  \    return FModuleManager::LoadModuleChecked <F" . uesource . "> (\"" . uesource . "\");\n
  \  }\n
  \  static inline bool IsAvailable() {\n
  \    return FModuleManager::Get().IsModuleLoaded (\"" . uesource . "\");\n
  \  }\n
  \  virtual void StartupModule() override;\n
  \  virtual void ShutdownModule() override;\n
  \};"

command UEMODULECP execute "setlocal nocindent | normal! i
  \#include \"" . uesource . ".h\"\n
  \#include \"Modules/ModuleManager.h\"\n
  \\n
  \#include \"Log.h\"\n
  \\n
  \void F" . uesource . "::StartupModule() {\n
  \  UE_LOG (Log" . uesource . ", Log, TEXT (\"" . uesource . " module starting up\"));\n
  \}\n
  \\n
  \void F" . uesource . "::ShutdownModule() {\n
  \  UE_LOG (Log" . uesource . ", Log, TEXT (\"" . uesource . " module shutting down\"));\n
  \}\n
  \\n
  \IMPLEMENT_PRIMARY_GAME_MODULE (F" . uesource . ", " . uesource . ", \"" . uesource . "\");"

let uemodule = expand ("%:h:h:t")
command UELOGH execute "setlocal nocindent | normal! i
  \#pragma once\n
  \\n
  \#include \"Logging/LogMacros.h\"\n
  \\n
  \DECLARE_LOG_CATEGORY_EXTERN (Log" . uemodule . ", All, All);"

command UELOGC execute "setlocal nocindent | normal! i
  \#include \"Log.h\"\n
  \\n
  \DEFINE_LOG_CATEGORY (Log" . uemodule . ");"

command UELOGL execute "normal! o
  \UE_LOG (Log" . uemodule . ", Log, TEXT (\"hello world\"));"

" ------------------------------------------------------------------------------
let &cpo= s:keepcpo
unlet s:keepcpo
